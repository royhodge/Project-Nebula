---?image=assets/image/blank.jpg&opacity=75
# Codename Nebula 

A Graphical Wallet to use with MultiChain,<br>
Built With Electron & Bootstrap.

---?image=assets/image/blank.jpg&opacity=75
### Warning!!! 

<blockquote>
Everything in this presentation is a work in progress and up for radical changes constantly.
You may load something up, and as soon as you refresh, it could be very different. 
We are a group of 3-6 and growing, please discuss major changes in the discord and follow best practices of making a PR that can be reviewed before being merged to master.
</blockquote>

---?image=assets/image/blank.jpg&opacity=75
# Join us on Discord 
## For The Latest Updates!

Link To Join Is Here: https://discord.gg/KKeekgT

@fa[arrows gp-tip](Press F to go Fullscreen)
@fa[microphone gp-tip](Press S for Speaker Notes)

---?image=assets/image/blank.jpg&opacity=75
## Wallet Main Features

- Connect To Multiple Blockchains Simultaneously |
- Native Blockchain Balances | 
  + Asset Balances Displayed |
- Custom Themes Need To Be Available |
- Safe & Secure Is Priority! |
- Custom Logo, TOC, and Footnotes |

---?image=assets/image/blank.jpg&opacity=75

@title[How It Works]

<p><span class="slide-title">How It Works</span></p>

```javascript
var http = require("http");

http.createServer(function (request, response) {
  request.on("end", function () {
    response.writeHead(200, {
      'Content-Type': 'text/plain'
    });
    response.end('Hello HTTP!');
  });
}).listen(8080);
```

@[1-7](We explain how we put it together.)
@[8-10](And, all of this without ever leaving the slideshow.)

---?image=assets/image/blank.jpg&opacity=75
## Wallet Support & Help

- [Discord Server for 24/7 Support](https://discord.gg/KKeekgT) |
  + [This GitPitch For Latest Published Updates](https://gitpitch.com/unibitproject/nebula) |
- [The Github Repo](https://github.com/UniBitProject/nebula/) |
- [User Documentation](https://unibit.gitbook.io/nebula/) |

---?image=assets/image/blank.jpg&opacity=75
## The Road Ahead

<div class="left">
    <i class="fa fa-user-secret fa-5x" aria-hidden="true"> </i><br>
    <a href="https://gitpitch.com/pro-features" class="pro-link">
    More details here.</a> 
</div>

<div class="right">
    <ul>
        <li>Password-Protection</li> 
        <li>Theme Opacity Settings</li> 
        <li>Drag and Drop Support</li> 
    </ul>
</div>

---?image=assets/image/blank.jpg&opacity=75

### Questions?

<br>

@fa[twitter gp-contact](@unibitlabs) 

@fa[github gp-contact](unibitlabs) 

---?image=assets/image/blank.jpg&opacity=75

@title[Get Started Beta Testing!]

### Beta Test The Latest Pre-Release's Here 
#### Then Submit Info Back For Constant Community Improvement!

<br>

- [Get To Work Beta Testing! @fa[gear gp-download]](https://discord.gg/KKeekgT) |
- [Submit A Review Here! @fa[user gp-download]](#) |

